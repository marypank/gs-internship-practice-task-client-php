# # SearchUserVotesResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\GSInternshipPracticeTaskClient\Dto\UserVote[]**](UserVote.md) |  | 
**meta** | [**\GSInternshipPracticeTaskClient\Dto\SearchPostsResponseMeta**](SearchPostsResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


