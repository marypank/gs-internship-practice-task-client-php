# # CreatePostRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_id** | **int** | Идентификатор пользователя | [optional] 
**title** | **string** | Строка с заголовком | [optional] 
**preview_text** | **string** | Preview текст | [optional] 
**text** | **string** | Текст статьи в HTML формате | [optional] 
**preview_img** | **string** | Preview изображение в виде ссылки | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


