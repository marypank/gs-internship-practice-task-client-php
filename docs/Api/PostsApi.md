# GSInternshipPracticeTaskClient\PostsApi

All URIs are relative to *http://localhost/api/v1/posts*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPost**](PostsApi.md#createPost) | **POST** /posts | Создание объекта типа Post
[**deletePost**](PostsApi.md#deletePost) | **DELETE** /posts/{id} | Удаление объекта типа Post
[**getPost**](PostsApi.md#getPost) | **GET** /posts/{id} | Получение объекта типа Post
[**patchPost**](PostsApi.md#patchPost) | **PATCH** /posts/{id} | Обновления части полей объекта типа Post
[**searchPosts**](PostsApi.md#searchPosts) | **POST** /posts:search | Поиск объектов типа Post



## createPost

> \GSInternshipPracticeTaskClient\Dto\PostResponse createPost($create_post_request)

Создание объекта типа Post

Создание объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new GSInternshipPracticeTaskClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_post_request = new \GSInternshipPracticeTaskClient\Dto\CreatePostRequest(); // \GSInternshipPracticeTaskClient\Dto\CreatePostRequest | 

try {
    $result = $apiInstance->createPost($create_post_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->createPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_post_request** | [**\GSInternshipPracticeTaskClient\Dto\CreatePostRequest**](../Model/CreatePostRequest.md)|  |

### Return type

[**\GSInternshipPracticeTaskClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deletePost

> \GSInternshipPracticeTaskClient\Dto\EmptyDataResponse deletePost($id)

Удаление объекта типа Post

Удаление объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new GSInternshipPracticeTaskClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deletePost($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->deletePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\GSInternshipPracticeTaskClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPost

> \GSInternshipPracticeTaskClient\Dto\PostResponse getPost($id)

Получение объекта типа Post

Получение объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new GSInternshipPracticeTaskClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getPost($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->getPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\GSInternshipPracticeTaskClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchPost

> \GSInternshipPracticeTaskClient\Dto\PostResponse patchPost($id, $patch_post_request)

Обновления части полей объекта типа Post

Обновления части полей объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new GSInternshipPracticeTaskClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_post_request = new \GSInternshipPracticeTaskClient\Dto\PatchPostRequest(); // \GSInternshipPracticeTaskClient\Dto\PatchPostRequest | 

try {
    $result = $apiInstance->patchPost($id, $patch_post_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->patchPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_post_request** | [**\GSInternshipPracticeTaskClient\Dto\PatchPostRequest**](../Model/PatchPostRequest.md)|  |

### Return type

[**\GSInternshipPracticeTaskClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchPosts

> \GSInternshipPracticeTaskClient\Dto\SearchPostsResponse searchPosts($search_posts_request)

Поиск объектов типа Post

Поиск объектов типа Post по id, user_id и по рейтингу (>=)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new GSInternshipPracticeTaskClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_posts_request = new \GSInternshipPracticeTaskClient\Dto\SearchPostsRequest(); // \GSInternshipPracticeTaskClient\Dto\SearchPostsRequest | 

try {
    $result = $apiInstance->searchPosts($search_posts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->searchPosts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_posts_request** | [**\GSInternshipPracticeTaskClient\Dto\SearchPostsRequest**](../Model/SearchPostsRequest.md)|  |

### Return type

[**\GSInternshipPracticeTaskClient\Dto\SearchPostsResponse**](../Model/SearchPostsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

