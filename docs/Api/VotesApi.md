# GSInternshipPracticeTaskClient\VotesApi

All URIs are relative to *http://localhost/api/v1/posts*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUserVote**](VotesApi.md#createUserVote) | **POST** /user-votes | Создание объекта типа UserVote
[**deleteUserVote**](VotesApi.md#deleteUserVote) | **DELETE** /user-votes/{id} | Удаление объекта типа UserVote
[**patchUserVote**](VotesApi.md#patchUserVote) | **PATCH** /user-votes/{id} | Обновления части полей объекта типа UserVote
[**searchUserVotes**](VotesApi.md#searchUserVotes) | **POST** /user-votes:search | Поиск объектов типа UserVote



## createUserVote

> \GSInternshipPracticeTaskClient\Dto\UserVoteResponse createUserVote($create_user_vote_request)

Создание объекта типа UserVote

Создание объекта типа UserVote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new GSInternshipPracticeTaskClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_user_vote_request = new \GSInternshipPracticeTaskClient\Dto\CreateUserVoteRequest(); // \GSInternshipPracticeTaskClient\Dto\CreateUserVoteRequest | 

try {
    $result = $apiInstance->createUserVote($create_user_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->createUserVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_user_vote_request** | [**\GSInternshipPracticeTaskClient\Dto\CreateUserVoteRequest**](../Model/CreateUserVoteRequest.md)|  |

### Return type

[**\GSInternshipPracticeTaskClient\Dto\UserVoteResponse**](../Model/UserVoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteUserVote

> \GSInternshipPracticeTaskClient\Dto\EmptyDataResponse deleteUserVote($id)

Удаление объекта типа UserVote

Удаление объекта типа UserVote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new GSInternshipPracticeTaskClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteUserVote($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->deleteUserVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\GSInternshipPracticeTaskClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchUserVote

> \GSInternshipPracticeTaskClient\Dto\UserVoteResponse patchUserVote($id, $patch_user_vote_request)

Обновления части полей объекта типа UserVote

Обновления части полей объекта типа UserVote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new GSInternshipPracticeTaskClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_user_vote_request = new \GSInternshipPracticeTaskClient\Dto\PatchUserVoteRequest(); // \GSInternshipPracticeTaskClient\Dto\PatchUserVoteRequest | 

try {
    $result = $apiInstance->patchUserVote($id, $patch_user_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->patchUserVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_user_vote_request** | [**\GSInternshipPracticeTaskClient\Dto\PatchUserVoteRequest**](../Model/PatchUserVoteRequest.md)|  |

### Return type

[**\GSInternshipPracticeTaskClient\Dto\UserVoteResponse**](../Model/UserVoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchUserVotes

> \GSInternshipPracticeTaskClient\Dto\SearchUserVotesResponse searchUserVotes($search_user_votes_request)

Поиск объектов типа UserVote

Поиск объектов типа UserVote по user_id и post_id

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new GSInternshipPracticeTaskClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_user_votes_request = new \GSInternshipPracticeTaskClient\Dto\SearchUserVotesRequest(); // \GSInternshipPracticeTaskClient\Dto\SearchUserVotesRequest | 

try {
    $result = $apiInstance->searchUserVotes($search_user_votes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->searchUserVotes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_user_votes_request** | [**\GSInternshipPracticeTaskClient\Dto\SearchUserVotesRequest**](../Model/SearchUserVotesRequest.md)|  |

### Return type

[**\GSInternshipPracticeTaskClient\Dto\SearchUserVotesResponse**](../Model/SearchUserVotesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

