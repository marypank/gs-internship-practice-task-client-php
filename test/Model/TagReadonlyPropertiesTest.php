<?php
/**
 * TagReadonlyPropertiesTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  GSInternshipPracticeTaskClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Rest API Practice Task 2
 *
 * Rest API Practice Task 2 Description
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace GSInternshipPracticeTaskClient;

use PHPUnit\Framework\TestCase;

/**
 * TagReadonlyPropertiesTest Class Doc Comment
 *
 * @category    Class
 * @description TagReadonlyProperties
 * @package     GSInternshipPracticeTaskClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class TagReadonlyPropertiesTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "TagReadonlyProperties"
     */
    public function testTagReadonlyProperties()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }
}
