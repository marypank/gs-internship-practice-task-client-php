<?php

namespace GSInternshipPracticeTaskClient;

class GSInternshipPracticeTaskClientProvider
{
    /** @var string[] */
    public static $apis = ['\GSInternshipPracticeTaskClient\Api\PostsApi', '\GSInternshipPracticeTaskClient\Api\VotesApi'];

    /** @var string[] */
    public static $dtos = [
        '\GSInternshipPracticeTaskClient\Dto\CreatePostRequest',
        '\GSInternshipPracticeTaskClient\Dto\CreateUserVoteRequest',
        '\GSInternshipPracticeTaskClient\Dto\EmptyDataResponse',
        '\GSInternshipPracticeTaskClient\Dto\Error',
        '\GSInternshipPracticeTaskClient\Dto\ErrorResponse',
        '\GSInternshipPracticeTaskClient\Dto\ModelInterface',
        '\GSInternshipPracticeTaskClient\Dto\PaginationTypeEnum',
        '\GSInternshipPracticeTaskClient\Dto\PatchPostRequest',
        '\GSInternshipPracticeTaskClient\Dto\PatchUserVoteRequest',
        '\GSInternshipPracticeTaskClient\Dto\Post',
        '\GSInternshipPracticeTaskClient\Dto\PostFillableProperties',
        '\GSInternshipPracticeTaskClient\Dto\PostIncludes',
        '\GSInternshipPracticeTaskClient\Dto\PostReadonlyProperties',
        '\GSInternshipPracticeTaskClient\Dto\PostResponse',
        '\GSInternshipPracticeTaskClient\Dto\RequestBodyCursorPagination',
        '\GSInternshipPracticeTaskClient\Dto\RequestBodyOffsetPagination',
        '\GSInternshipPracticeTaskClient\Dto\RequestBodyPagination',
        '\GSInternshipPracticeTaskClient\Dto\ResponseBodyCursorPagination',
        '\GSInternshipPracticeTaskClient\Dto\ResponseBodyOffsetPagination',
        '\GSInternshipPracticeTaskClient\Dto\ResponseBodyPagination',
        '\GSInternshipPracticeTaskClient\Dto\SearchPostsRequest',
        '\GSInternshipPracticeTaskClient\Dto\SearchPostsResponse',
        '\GSInternshipPracticeTaskClient\Dto\SearchPostsResponseMeta',
        '\GSInternshipPracticeTaskClient\Dto\SearchUserVotesRequest',
        '\GSInternshipPracticeTaskClient\Dto\SearchUserVotesResponse',
        '\GSInternshipPracticeTaskClient\Dto\Tag',
        '\GSInternshipPracticeTaskClient\Dto\TagFillableProperties',
        '\GSInternshipPracticeTaskClient\Dto\TagReadonlyProperties',
        '\GSInternshipPracticeTaskClient\Dto\UserVote',
        '\GSInternshipPracticeTaskClient\Dto\UserVoteEnum',
        '\GSInternshipPracticeTaskClient\Dto\UserVoteFillableProperties',
        '\GSInternshipPracticeTaskClient\Dto\UserVoteReadonlyProperties',
        '\GSInternshipPracticeTaskClient\Dto\UserVoteRequiredRequestProperties',
        '\GSInternshipPracticeTaskClient\Dto\UserVoteResponse',
    ];

    /** @var string */
    public static $configuration = '\GSInternshipPracticeTaskClient\Configuration';
}
